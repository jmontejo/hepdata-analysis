# Hepdata analysis

Collection of notebooks and useful scripts to analyze metadata from HepData

## Getting inputs

The [get_access_count.ipynb](get_access_count.ipynb) notebook provided by Graeme Watt uses the json API to query all info and dumps into a csv. An older web scraper with a similar functionality in a much more ugly way is in [scrap_hepdata.py](scrap_hepdata.py).

## Analysing inputs

The [HepData analysis.ipynb](HepData analysis.ipynb) notebook analyses the csv and dumps some interesting statistics

## Output summary

After correcting for an estimate of the automated downloads, more than 80% of our records are actively used. With a more conservative definition of the "pedestal" 63% of our records are used.
This picture changes significantly across groups, with the nominal 82% being actually 90% (77%) for SUSY (non-SUSY) papers

SUSY is proud owner of all of the top 5% except for one scalar leptoquark paper and the CalRatio paper :)

