#!/usr/bin/env python3
import argparse
import logging
import os
import json

scriptname = os.path.basename(__file__)
log = logging.getLogger(scriptname)

import scrapy
from scrapy.crawler import CrawlerProcess


class HepDataSpider(scrapy.Spider):
    start_urls = [f'http://www.hepdata.net/search/?q=search&page={index}&collaboration=ATLAS&size=100' for index in range(1,5)] 
    name = 'hepdata_spider'

    def parse(self, response):

        record_selector = '.record-header'
        counter = 0
        print("WTF",response)
        for record in response.css(record_selector):
            print("In loop", counter)
            counter += 1
            link_selector = 'a ::attr(href)'
            next_link = record.css(link_selector).extract_first()
            json_link = f"https://www.hepdata.net/{next_link}?format=json&light=true"
            if next_link:
                print ("#", next_link)
                yield scrapy.Request(
                    response.urljoin(json_link),
                    callback=self.parse_record
                )

    def parse_record(self, response):
        jsonresponse = json.loads(response.text)
        yield jsonresponse

def main():
    args = parse_args()

    #Do your magic
    log.info(f"Starting {scriptname}")
    process = CrawlerProcess(
        settings={
            "FEEDS": {
                "hepdata_output.json": {"format": "json"},
            },
        }
    )
    
    process.crawl(HepDataSpider)
    process.start()  # the script will block here until the crawling is finished

def parse_args():
    logchoices = ('DEBUG','INFO','WARNING','ERROR','CRITICAL')
    parser = argparse.ArgumentParser()
    parser.add_argument('--log', choices=logchoices, default='INFO', help='Set log level')
    
    args = parser.parse_args()
    logging.basicConfig(level=args.log)
    return args

if __name__ == "__main__":
    main()
